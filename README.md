# LostnSearch

## Inspiration
We often,  most pronounced in rural areas, find people getting separated from their loved ones, be it during large social event gatherings or any other misfortune. Seeing such heartbreaking stories on social and in newspapers inspired us to make a platform to report and find missing people.

## What it does
Our platform takes data of a missing person, reported to us by the well-wisher of the lost, and matches with the existing database using ElasticSearch.

## How we built it
Used Django, DRF, Elastic, and love

## Challenges we ran into
Learning ElasticSearch comes with a great learning curve. We went through numerous guides and StackOverflow posts to help integrate ElasticSearch with our model but all went in vain. We were on the verge of giving up but in the end, decided to go through the official documentation of ElasticSearch. To our surprise, the documentation was very well written and in no time we were up and running again.

## Accomplishments that we're proud of
The accomplishment that we are most proud of is finally connecting our platform with Elasticsearch App Search, that too in a remarkable 2 days. Never in our wildest dreams, we imagined bringing out a working model of such magnitude in such a short duration of time. This has immensely motivated us to get deeper into development and continue working on other projects.

## What's next for LostnSearch
Need to add more features like if someone found a listed person he/she can contact the reported person and need to add some layers of authentication and authorization but time restricts.
We also would like to involve lost Pets, as at times Pets can get troublesome and might get lost. A platform such as ours would be a great way to help owners find their lost pets.
In the fairly distant future, we also plan to use Apple's existing network to use LostNSearch with non-living products using Location tracking devices and continue to grow our platform.