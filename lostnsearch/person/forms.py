from django.forms import ModelForm
from django import forms

from lostnsearch.enums import GENDER
from person.models import Person, Reported_By

class Reported_ByForm(ModelForm):
    class Meta:
        model   = Reported_By
        fields  = '__all__'
        labels  = {
            'name'  : ('Your Name'),
            'phone' : ('Your Phone'),
            'email' : ('Your Email'),
        }

class PersonForm(ModelForm):
    class Meta:
        model   = Person
        exclude = ('reported_by', 'status')
        labels  = {
            'name': 'Missing Person Name',
        }