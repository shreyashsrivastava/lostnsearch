from django.urls import path, include
from django.views.generic import TemplateView

from . import views

app_name = 'person'

urlpatterns = [
    path('', views.index, name='home'),
    path('missing', views.missing_form, name="missing"),
    path('generate/<int:qty>', views.fake_data_generator, name="generate"),
    path('api/', include('person.api.urls')),
]
