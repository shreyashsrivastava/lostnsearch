from django.http.response import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.contrib import messages
import json

from person.models import Person, Reported_By
from person.forms import Reported_ByForm, PersonForm

def index(request):
    persons = Person.missing.all()
    context = {
        'title':'LostnSearch',
        'persons': persons,
    }
    return render(request, 'person/index.html', context)

def missing_form(request):
    if request.method == 'POST':
        reported_by_form    = Reported_ByForm(request.POST)
        missing_person_form = PersonForm(request.POST, request.FILES)
        
        if reported_by_form.is_valid() and missing_person_form.is_valid():
            reported_by     = reported_by_form.save()
            missing_person  = missing_person_form.save(commit=False)
            missing_person.reported_by = reported_by
            missing_person.status = 'missing'
            missing_person.save() 

            messages.success(request, 'Successfully posted!')

            return HttpResponseRedirect(reverse('person:home'))
        else:
            messages.error(request, "There's some problem with the inputs!")
            return HttpResponseRedirect(reverse('person:missing'))
    else:
        reported_by_form    = Reported_ByForm()
        missing_person_form = PersonForm()
        context = {
            'reported_by_form':reported_by_form,
            'missing_person_form':missing_person_form,
            'title':'Missing Form',
        }
        return render(request, 'person/missing_form.html', context)

from faker import Faker
import random
from lostnsearch.enums import GENDER, HUMAN_SKIN_COLOR, PERSON_STATUS
def fake_data_generator(request, qty):
    faker = Faker()
    for i in range(qty):
        r = Reported_By.objects.create(
            name = faker.name(),
            email = faker.email(),
            phone = faker.msisdn()[:9]
        )
        Person.objects.create(
            reported_by = r,
            name = faker.name(),
            gender = GENDER[random.randint(0,2)][0],
            resident_city = faker.city(),
            resident_country = faker.country(),
            missing_city = faker.city(),
            missing_date = faker.date(),
            DOB = faker.date(),
            age = random.randint(10, 60),
            height = random.randint(100, 200),
            complexion = HUMAN_SKIN_COLOR[random.randint(0,5)][0],
            birthmark = faker.sentence(nb_words=2),
            physique = faker.sentence(nb_words=2),
            unique_identifier = faker.sentence(nb_words=2),
            hair_color = faker.sentence(nb_words=1, ext_word_list=['black', 'white', 'blonde', 'red']),
            cause = faker.sentences()[0],
            status = 'missing',
        )

    messages.success(request, "Fake data created")
    return HttpResponseRedirect(reverse('person:home'))
