from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from person.api.serializers import PersonSerializer

from person.models import Person

app_search = settings.APP_SEARCH

@receiver(post_save, sender=Person)
def index_elastic_search(sender, instance, created, **kwargs):
    if created:
        from django.core import serializers
        serialized_person = serializers.serialize('json', [instance], ensure_ascii=False)[1:-1]
        app_search.index_documents(
            engine_name="missing-person",
            documents=serialized_person,
        )