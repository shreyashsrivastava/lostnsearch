from django.contrib import admin
from .models import Person, Reported_By
# Register your models here.

admin.site.register(Person)
admin.site.register(Reported_By)
