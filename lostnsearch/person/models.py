from django.db import models
from lostnsearch.enums import GENDER, HUMAN_SKIN_COLOR, PERSON_STATUS

class Reported_By(models.Model):
    name    = models.CharField(max_length=50)
    phone   = models.PositiveIntegerField() #India specific, will further add cuontry code and validators
    email   = models.EmailField(max_length=254)

    def __str__(self):
        return self.name
    
class PersonMissingManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status='missing').order_by('-created_at')

class PersonFoundManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status='found').order_by('-created_at')

def upload_directory_path(instance, filename):
    return 'person_{}/{}'.format(instance.reported_by.id, filename)


class Person(models.Model):
    reported_by         = models.ForeignKey(Reported_By, related_name='reported_by', on_delete=models.CASCADE)
    name                = models.CharField(max_length=50)
    image               = models.ImageField(upload_to=upload_directory_path, default='default.jpg', null=True, blank=True)
    gender              = models.CharField(max_length=50, choices=GENDER)
    resident_city       = models.CharField(max_length=50, null=True, blank=True)
    resident_country    = models.CharField(max_length=50, null=True, blank=True)
    missing_city        = models.CharField(max_length=50, null=True, blank=True)
    missing_date        = models.DateField(null=True, blank=True)
    DOB                 = models.DateField(null=True, blank=True)
    age                 = models.PositiveIntegerField()
    height              = models.PositiveIntegerField(null=True, blank=True)
    complexion          = models.CharField(max_length=50, choices=HUMAN_SKIN_COLOR, null=True, blank=True)
    birthmark           = models.CharField(max_length=100, null=True, blank=True)
    physique            = models.CharField(max_length=50, null=True, blank=True)
    unique_identifier   = models.CharField(max_length=100, null=True, blank=True)
    hair_color          = models.CharField(max_length=50, null=True, blank=True)
    cause               = models.CharField(max_length=200, null=True, blank=True)
    status              = models.CharField(max_length=50, choices=PERSON_STATUS)

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    objects     = models.Manager()
    missing     = PersonMissingManager()
    found       = PersonFoundManager()

    def __str__(self):
        return self.name