from django.http.response import HttpResponseRedirect
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics
from django.urls import reverse
from django.contrib import messages
from django.conf import settings

from person.models import Person, Reported_By
from person.api.serializers import PersonSerializer

app_search = settings.APP_SEARCH

print(app_search.list_engines())

class index(generics.ListAPIView):
    queryset = Person.missing.all()
    serializer_class = PersonSerializer

def AppSearchIndexDocuments(request):
    import json
    persons = Person.missing.all()
    serialized_person = PersonSerializer(persons).data
    # from django.core import serializers
    # persons = Person.missing.all()
    # serialized_person = serializers.serialize('json', persons)
    
    app_search.index_documents(
        engine_name="missing-person",
        documents=serialized_person,
    )
    messages.success(request, 'App search indexed successfully')
    return HttpResponseRedirect(reverse('person:home'))
