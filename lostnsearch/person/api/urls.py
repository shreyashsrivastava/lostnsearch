from django.urls import path, include
from django.views.generic import TemplateView

from . import views

app_name = 'person_api'

urlpatterns = [
    path('', views.index.as_view(), name='index'),
    path('indexsearch', views.AppSearchIndexDocuments, name='index_app_search')
]
