from rest_framework import serializers
from person.models import Person, Reported_By

class Reported_BySerializer(serializers.ModelSerializer):
    class Meta:
        model   = Reported_By
        fields  = '__all__'

class PersonSerializer(serializers.ModelSerializer):
    
    reported_by         = Reported_BySerializer()
    
    class Meta:
        model = Person
        fields = '__all__'
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    