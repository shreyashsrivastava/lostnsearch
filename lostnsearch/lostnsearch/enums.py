HUMAN_SKIN_COLOR = (
    ('Light, pale white', 'Light, pale white'),
    ('White, fair', 'White, fair'),
    ('Medium white to light brown', 'Medium white to light brown'),
    ('Olive, moderate brown', 'Olive, moderate brown'),
    ('Brown, dark brown', 'Brown, dark brown'),
    ('Very dark brown to black	', 'Very dark brown to black	')
)

GENDER = (
    ('male', 'Male'),
    ('female', 'Female'),
    ('others', 'Others'),
)

PERSON_STATUS = (
    ('missing', 'Missing'),
    ('found', 'Found'),
)
